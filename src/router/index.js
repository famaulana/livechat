import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/verification',
      name: 'verification',
      component: () => import('../views/VerificationView.vue')
    },
    {
      path: '/chat',
      name: 'about',
      component: () => import('../views/ChatView.vue')
    }
  ]
})

export default router
